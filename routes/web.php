<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Web\CatalogController;
use App\Http\Controllers\Web\ImageController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Home', [
        'canLogin' => Route::has('web.login'),
        'canRegister' => Route::has('web.register'),
    ]);
})->name('home');

Route::get('lorem-ipsum', function () {
    return Inertia::render('LoremIpsum');
})->name('lorem-ipsum');


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::prefix('catalog')
    ->as('catalog.')
    ->group(function () {
        Route::get('/', [CatalogController::class, 'catalog'])->name('index');
    });

Route::get('/images/{path}', [ImageController::class, 'get'])->where('path', '.*')->name('images');

require __DIR__ . '/auth.php';
