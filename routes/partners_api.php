<?php

use App\Http\Controllers\PartnersApi\CurrencyController;
use App\Http\Controllers\PartnersApi\ProductsController;
use Illuminate\Support\Facades\Route;

Route::get('/', fn () => response()->json('ok'));
Route::get('/currencies', [CurrencyController::class, 'list'])->name('currencies');
Route::prefix('products')
    ->as('products.')
    ->group(function () {
        Route::post('create', [ProductsController::class, 'create'])->name('create');
        Route::post('{product}/save', [ProductsController::class, 'save'])->name('save');
    });
