<?php

use App\Http\Controllers\Partners\ProductsController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', fn () => Inertia::render('Panel/Orders'))->name('index');
Route::prefix('/products')
    ->as('products.')
    ->group(function () {
        Route::get('/', [ProductsController::class, 'index'])->name('index');
        Route::get('create', [ProductsController::class, 'create'])->name('create');
        Route::get('edit/{product}', [ProductsController::class, 'edit'])->name('edit');
    });
