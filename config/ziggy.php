<?php

return [
    'groups' => [
        'public' => ['web.*', 'api.*'],
        'authorized' => ['profile.*'],
        'partner' => ['partner-panel.*', 'partner-api.*'],
    ],
];
