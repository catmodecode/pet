import { ProductInterface, imageInterface } from "@/types";

export class Product implements ProductInterface {
    public id: number;
    public images: imageInterface[];
    public name: string;
    public price: number;
    public currency: string;

    constructor(
        id: number = 0,
        images: imageInterface[],
        name: string,
        price: number,
        currency: string,
    ) {
        this.id = id
        this.images = images ?? [];
        this.name = name;
        this.price = price;
        this.currency = currency;
    }

    public static copyFrom ({
        id = 0,
        images,
        name,
        price,
        currency,
    }: ProductInterface) {
        return new Product(
            id,
            images,
            name,
            price,
            currency
        )
    }

    public getImages (): imageInterface[] {
        return this.images;
    }

    public getName (): string {
        return this.name;
    }

    public getPrice (): number {
        return this.price;
    }

    public getCurrency (): string {
        return this.currency;
    }
}

export class PublicProduct implements ProductInterface {
    public id: number;
    public images: imageInterface[];
    public name: string;
    public price: number;
    public currency: string;

    constructor(
        images: Image[],
        name: string,
        price: number,
        currency: string,
    ) {
        this.id = 0
        this.images = images ?? [];
        this.name = name;
        this.price = price;
        this.currency = currency;
    }

    public getImages (): imageInterface[] {
        return this.images;
    }

    public getName (): string {
        return this.name;
    }

    public getPrice (): number {
        return this.price;
    }

    public getCurrency (): string {
        return this.currency;
    }
}

export class Image implements imageInterface {
    id: number;
    url: string;
    file: Blob | undefined;

    constructor({
        id = 0,
        url,
        file,
    }: {
        id: number,
        url: string,
        file: Blob,
    }) {
        this.id = id
        this.url = url
        this.file = file
    }
}
