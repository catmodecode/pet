import { Image } from '@/classes';
import { ReactElement } from 'react';
import { Config } from 'ziggy-js';

export interface User {
    id: number;
    name: string;
    email: string;
    email_verified_at: string;
    roles: string[]
}

export type PageProps<T extends Record<string, unknown> = Record<string, unknown>> = T & {
    auth: {
        user: User;
    };
    ziggy: Config & { location: string };
};

export interface LinkInterface {
    active: boolean,
    label: string|ReactElement,
    url: string,
}

export interface LengthAwarePaginatorInterface<Type> {
    current_page: number,
    data: Type[],
    first_page_url: string,
    from: number,
    last_page: number,
    last_page_url: string,
    links: LinkInterface[],
    next_page_url: string,
    path: string,
    per_page: number,
    prev_page_url: string,
    to: number,
    total: number,
}

export interface ProductInterface {
    id: number,
    images: imageInterface[],
    name: string,
    price: number,
    currency: string,

    getImages (): imageInterface[]
    getName (): string
    getPrice (): number
    getCurrency (): string
}

export interface CurrencyInterface {
    code: string,
    name: string,
    sign: string,
}

export interface imageInterface {
    id: number,
    url: string,
}
