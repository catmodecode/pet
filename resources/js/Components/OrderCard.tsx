import { ReactNode } from "react";
import SkeletonBlock from "./SkeletonBlock"
import ButtonGreen from "./ButtonGreen";
import ButtonRed from "./ButtonRed";

export class Order {
    public date: Date;
    public image: string;
    public status: string;
    public address: string;

    constructor(
        date: Date,
        image: string,
        status: string,
        address: string,
    ) {
        this.date = date
        this.image = image
        this.status = status
        this.address = address
    }

    public getImage (): string {
        return this.image
    }

    public getDate (): string {
        return this.date.toDateString()
    }

    public getStatus (): string {
        return this.status
    }

    public getAddress (): string {
        return this.address
    }
}

export default function OrderCard ({ order }: { order: Order | undefined }) {
    const data = order
        ? {
            image: (<img className="h-48 w-full object-cover md:w-48" src={order.getImage()} />),
            date: (<>{order.getDate()}</>),
            status: (<>{order.getStatus()}</>),
            address: (<>{order.getAddress()}</>),
        }
        : {
            image: (<SkeletonBlock className="h-48 w-full object-cover md:w-48" children={(<></>)} />),
            date: undefined,
            status: undefined,
            address: undefined
        }

    const date = order

    return (
        <div className="md:flex border-solid border-white border-2 rounded-md">
            <SkeletonBlock className="md:flex-shrink-0">
                {data.image}
            </SkeletonBlock>
            <div className="p-4 w-full">
                <div className="flex justify-between space-x-4 pb-3">
                    <ButtonGreen onClick={() => { }} className=" w-1/2">
                        Accept
                    </ButtonGreen>
                    <ButtonRed onClick={() => { }} className=" w-1/2">
                        Decline
                    </ButtonRed>
                </div>
                <SkeletonBlock className="tracking-wide text-sm min-w-14 min-h-4 text-orange-200 font-semibold">{data.date}</SkeletonBlock>
                <SkeletonBlock className="block mt-1 text-lg min-w-14 min-h-4 leading-tight font-medium text-white">{data.status}</SkeletonBlock>
                <SkeletonBlock className="mt-2 text-gray-400 min-w-14 min-h-4">{data.address}</SkeletonBlock>
            </div>
        </div>
    )
}