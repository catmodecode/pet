import axios from "axios";
import Dropdown from "./Dropdown";
import Menu from "./Menu";
import ResponsiveNavLink from "./ResponsiveNavLink";

export default function PartnerMenu () {
    return (
        <Dropdown>
            <Dropdown.Trigger>
                <span className="inline-flex rounded-md">
                    <button
                        type="button"
                        className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 dark:text-gray-400 bg-white dark:bg-gray-800 hover:text-gray-700 dark:hover:text-gray-300 focus:outline-none transition ease-in-out duration-150"
                    >
                        Menu
                        <Menu />
                    </button>
                </span>
            </Dropdown.Trigger>
            <Dropdown.Content>
                <Dropdown.Link href={route('partner-panel.index')}>Orders</Dropdown.Link>
                <Dropdown.Link href={route('partner-panel.products.index')}>Products</Dropdown.Link>
            </Dropdown.Content>
        </Dropdown>
    )
}