import { ReactNode } from "react"
import Button from "./Button"

export default function ButtonRed ({ className, children, onClick }:
    { className: string, children: ReactNode, onClick: () => void }) {
    const classes = 'bg-red-800 ' + (className ?? '')
    return (
        <Button onClick={onClick} className={classes}>
            {children}
        </Button>
    )
}