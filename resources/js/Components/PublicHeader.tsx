import { Link } from "@inertiajs/react";
import ApplicationLogo from "./ApplicationLogo";
import NavLink from "./NavLink";
import HasRole from "@/helpers/HasRole";
import { User } from "@/types";
import { useState } from "react";

export default function PublicHeader ({ user }: { user: User | null }) {
    const [open, setOpen] = useState(false);
    const mainLeft = [
        { routeName: 'web.home', title: (<><ApplicationLogo className=" max-w-6" />Home</>) },
        { routeName: 'web.lorem-ipsum', title: (<>About</>) },
        { routeName: 'web.lorem-ipsum', title: (<>Contacs</>) },
        {
            routeName: 'web.lorem-ipsum', title: (<>
                <svg className=" w-5 mr-1 inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="#999" d="M256 32C114.6 32 .0272 125.1 .0272 240c0 49.63 21.35 94.98 56.97 130.7c-12.5 50.37-54.27 95.27-54.77 95.77c-2.25 2.25-2.875 5.734-1.5 8.734C1.979 478.2 4.75 480 8 480c66.25 0 115.1-31.76 140.6-51.39C181.2 440.9 217.6 448 256 448c141.4 0 255.1-93.13 255.1-208S397.4 32 256 32z"></path>
                </svg>
                Chat
            </>)
        },
    ]
    const mainRight = [
        { routeName: 'web.lorem-ipsum', title: (<>help</>) },
    ]

    if (user) {
        if (HasRole(user?.roles, 'partner')) {
            mainRight.push({ routeName: 'partner-panel.index', title: (<>Partner Panel</>) })
        }
        if (HasRole(user?.roles, 'root') || HasRole(user?.roles, 'admin')) {
            mainRight.push({ routeName: 'dashboard', title: (<>Control Panel</>) })
        }
        mainRight.push({ routeName: 'web.profile.edit', title: (<>Profile</>) })
    } else {
        mainRight.push({ routeName: 'web.login', title: (<>Log in</>) })
        mainRight.push({ routeName: 'web.register', title: (<>Register</>) })
    }

    const catalog = [
        { routeName: 'web.catalog.index', title: (<>Catalog</>) },
        { routeName: 'web.lorem-ipsum', title: (<>Gifts</>) },
        { routeName: 'web.lorem-ipsum', title: (<>Sweets</>) },
        { routeName: 'web.lorem-ipsum', title: (<>Flowers</>) },
        { routeName: 'web.lorem-ipsum', title: (<>Funeral</>) },
        { routeName: 'web.lorem-ipsum', title: (<>Baloons</>) },
    ]
    return (
        <>
            <header>
                <div className="flex flex-col">
                    <nav className="flex bg-black max-w-7xl items-center justify-end p-2 lg:px-8" aria-label="Global">
                        <div className="flex lg:hidden">
                            <button onClick={() => setOpen(!open)} type="button" className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700">
                                <ApplicationLogo className=" max-w-6" />
                                <span className="sr-only">Open main menu</span>
                                <svg className="h-6 w-6" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                                </svg>
                            </button>
                        </div>
                        <div className="hidden lg:flex lg:gap-x-12">
                            {mainLeft.map(({ routeName, title }, index) => (
                                <NavLink key={index} active={route().current() == routeName} href={route(routeName)}>
                                    {title}
                                </NavLink>
                            ))}
                        </div>
                        <div className="hidden lg:flex lg:flex-1 lg:justify-end">
                            {mainRight.map(({ routeName, title }, index) => (
                                <NavLink key={index} active={route().current() == routeName} href={route(routeName)}>{title}</NavLink>
                            ))}
                        </div>
                    </nav>
                    <nav className="hidden lg:flex lg:max-w-7xl lg:items-center lg:justify-between lg:p-2 lg:px-8" aria-label="Global">
                        {catalog.map(({ routeName, title }, index) =>
                            (<NavLink key={index} active={route().current() == routeName} href={route(routeName)}>{title}</NavLink>)
                        )}
                    </nav>
                </div>
                <div className={"lg:hidden " + (open ? 'visible' : 'hidden')} role="dialog" aria-modal="true">
                    <div className="fixed inset-0 z-10"></div>
                    <div className="fixed inset-y-0 right-0 z-10 w-full overflow-y-auto bg-black px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
                        <div className="flex items-center justify-end">
                            <button onClick={() => setOpen(false)} type="button" className="-m-2.5 rounded-md p-2.5 text-gray-700">
                                <span className="sr-only">Close menu</span>
                                <svg className="h-6 w-6" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                        </div>
                        <div className="mt-6 flow-root">
                            <div className="-my-6 divide-y divide-gray-100/10">
                                <div className="py-6">
                                    {mainRight.map(({ routeName, title }, index) => (
                                        <Link className="-mx-3 flex flex-row rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-100" key={index} href={route(routeName)}>
                                            {title}
                                        </Link>
                                    ))}
                                </div>
                                <div className="space-y-2 py-6">
                                    {mainLeft.map(({ routeName, title }, index) => (
                                        <Link className="-mx-3 flex flex-row rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-100" key={index} href={route(routeName)}>
                                            {title}
                                        </Link>
                                    ))}
                                </div>
                                <div className="py-6">
                                    <div className="text-gray-300 py-4">
                                        Products
                                    </div>
                                    {catalog.map(({ routeName, title }, index) => (
                                        <Link className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-200" key={index} href={route(routeName)}>
                                            {title}
                                        </Link>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}