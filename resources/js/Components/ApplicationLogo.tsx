import { PageProps } from "@/types";

export default function ApplicationLogo ({ className }: {className: string}) {
    return (
        <img className={className} src="/images/logo-small.webp" />
    );
}
