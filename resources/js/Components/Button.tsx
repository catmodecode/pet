import { ReactNode } from "react"

export default function Button (
    { className, children, onClick }:
        { className: string, children: ReactNode, onClick: () => void }
) {
    if (onClick == undefined) {
        onClick = () => {}
    }
    const classes = 'h-6 w-38 underline cursor-pointer text-center text-gray-300 ' + (className ?? '')
    return (
        <div onClick={() => onClick()} className={classes}>
            {children}
        </div>
    )
}