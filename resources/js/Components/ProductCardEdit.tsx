import { Image, Product } from "@/classes";
import axios, { AxiosResponse } from "axios";
import ButtonGreen from "./ButtonGreen";
import { CurrencyInterface, ProductInterface, imageInterface } from "@/types";
import { useState } from "react";
import { swalError } from "@/helpers/SweetAlert";
import { useForm } from "@inertiajs/react";
import TextInput from "./TextInput";
import PrimaryButton from "./PrimaryButton";

export default function ProductCardEdit ({ product, className = undefined }: { product: ProductInterface | undefined, className: string | undefined }) {
    const classes = "editor w-full flex flex-col text-gray-800 border border-gray-300 p-4 shadow-lg max-w-2xl " +
        (className ? className : '')

    const images: Image[] = []
    if (product !== undefined) {
        product = Product.copyFrom(product);
        images.push(...product.getImages().map((image: imageInterface) => new Image({
            id: image.id,
            url: image.url,
            file: new Blob(),
        })))
    }
    console.log(product);


    const {
        data,
        setData,
        post,
        clearErrors,
        processing,
        reset,
        errors,
    } = useForm({
        images: images,
        deletedImages: [],
        title: product?.getName() ?? '',
        price: product?.price ?? '',
        currency: product?.currency ?? '',
    });

    const [currencies, setCurrencies] = useState<CurrencyInterface[]>([]);
    if (!currencies.length) {
        axios.get(route('partner-api.currencies')).then(
            (response: AxiosResponse) => {
                setCurrencies(response.data)
            }
        )
    }

    const saveProduct = function () {
        if (product === undefined) {
            createNew()
        } else {
            saveChanges()
        }
    }

    const createNew = function () {
        post(route('partner-api.products.create'), {
            onBefore: () => {
            },
            onError: () => {
                console.log('error!');

            }
        })
    }

    const saveChanges = function () {
        post(route('partner-api.products.save', { product: product?.id }), {
            onBefore: () => {
            },
            onError: () => {
                console.log('error!');

            }
        })
    }

    const newImage = function (event: any) {
        console.log(event.target.files);

        const file: any = Array.from(event.target.files)[0];
        const image = new Image({
            id: 0,
            file: file,
            url: URL.createObjectURL(file),
        })
        setData('images', [...data.images, image])
        console.log(data.images);

    }

    return (
        <div className={classes}>
            <div className="flex justify-around space-x-4 flex-wrap">
                {
                    data.images.map(
                        (image: Image) => (
                            <img className="h-auto max-w-52 rounded-lg m-4" key={image.url} src={image.url} />
                        )
                    )
                }
            </div>

            <div className=" text-red-500">
                {errors.images}
            </div>
            <div className="icons flex text-gray-500 m-2">
                <label id="select-image">
                    <svg className="mr-2 cursor-pointer hover:text-gray-700 border rounded-full p-1 h-7" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
                    </svg>
                    <input hidden type="file" onChange={newImage} />

                </label>
                <div className="count ml-auto text-gray-400 text-xs font-semibold">0/300</div>
            </div>
            <div className="count text-gray-300 text-md font-semibold">Product title</div>
            <TextInput
                id="title"
                type="title"
                name="title"
                value={data.title}
                onChange={(v) => setData('title', v.target.value)}
                className={"title bg-gray-100 border border-gray-300 p-2 mb-4 outline-none" + (errors.title ? ' border-red-500' : '')}
                spellCheck="false"
                isFocused
                placeholder="Product title"
            />
            <div className=" text-red-500">
                {errors.title}
            </div>
            <div className="count text-gray-300 text-md font-semibold">Price</div>
            <TextInput
                id="price"
                type="price"
                name="price"
                value={data.price}
                onChange={(v) => setData('price', v.target.value)}
                className="bg-gray-100 border border-gray-300 p-2 mb-4 outline-none"
                spellCheck="false"
                placeholder="Price"
            />
            <div className=" text-red-500">
                {errors.price}
            </div>
            {
                currencies
                    ? (<>
                        <div className="count text-gray-300 text-md font-semibold">Currency</div>
                        <select onChange={v => setData('currency', currencies?.find(currency => currency.sign == v.target.value)?.code ?? '')} name="currencies" id="currencies" className="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm">
                            <option key="none" value="">-</option>
                            {
                                currencies.map((currency: CurrencyInterface) => (
                                    <option key={currency.sign} value={currency.sign}>{currency.name}</option>
                                ))
                            }
                        </select>
                    </>)
                    : null
            }

            <div className=" text-red-500">
                {errors.currency}
            </div>
            <div className=" flex flex-row justify-end mt-4">
                <PrimaryButton disabled={processing} onClick={() => saveProduct()} >Save</PrimaryButton>
            </div>
        </div>
    )
}
