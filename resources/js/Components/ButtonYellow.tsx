import { ReactNode } from "react"
import Button from "./Button"

export default function ButtonYellow ({ className, children, onClick }:
    { className: string, children: ReactNode, onClick: () => void }) {
    const classes = 'bg-yellow-800 ' + (className ?? '')
    return (
        <Button onClick={onClick} className={classes}>
            {children}
        </Button>
    )
}