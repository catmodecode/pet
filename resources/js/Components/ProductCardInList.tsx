import { ProductInterface } from "@/types";
import ButtonRed from "./ButtonRed";
import ButtonYellow from "./ButtonYellow";
import SkeletonBlock from "./SkeletonBlock";
import { Product } from "@/classes";
import { Link } from "@inertiajs/react";

export default function ProductCardInList ({ product }: { product: Product | undefined }) {
    const data = product
        ? {
            id: product.id,
            image: product.getImages().length
                ? (<img src={product.getImages()[0].url} className="h-full w-full object-cover object-center lg:h-full lg:w-full" />)
                : '',
            name: (<span>{product?.getName()}</span>),
            price: (<span>{product.getPrice()} {product.getCurrency()}</span>),
        }
        : {
            id: 0,
            image: (<img className="h-32 w-full object-cover object-center" />),
            name: undefined,
            price: undefined,
        }

    return (
        <div className="group relative border-2 border-solid rounded-md flex flex-col justify-between">
            <SkeletonBlock className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
                {data.image}
            </SkeletonBlock>
            <div className="mt-4 flex justify-between pb-16">
                <div>
                    <h3 className="text-sm text-white-700">
                        <Link href={route('partner-panel.products.edit', { product: data.id })}>
                            <span aria-hidden="true" className="absolute inset-0"></span>
                            <SkeletonBlock className="w-16 h-5">
                                {data.name}
                            </SkeletonBlock>
                        </Link>
                    </h3>
                </div>
                <div className="text-sm font-medium text-white-900">
                    <SkeletonBlock className="w-16 h-5">{data.price}</SkeletonBlock>
                </div>
            </div>
        </div>
    )
}