import { ReactNode } from "react";

export default function SkeletonBlock ({ className, children }: { className: string | undefined, children: ReactNode | undefined }) {
    className += children ? '' : ' animate-pulse bg-slate-300';
    return (
        <div className={className}>{children}</div>
    )
}