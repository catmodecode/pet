import NavLink from "@/Components/NavLink";
import PublicHeader from "@/Components/PublicHeader";
import HasRole from "@/helpers/HasRole";
import { PageProps } from "@/types";
import { Link } from "@inertiajs/react";
import { ReactNode } from 'react';


export default function Public ({ children, auth, ziggy }: PageProps<{ children: ReactNode }>) {    
    return (
        <div className="site">
            <PublicHeader user={auth?.user} />
            <div className="container  with-background text-gray-800 dark:text-gray-200">
                {children}
            </div>
        </div>

    )
}