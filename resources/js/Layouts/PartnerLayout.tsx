import PartnerMenu from "@/Components/PartnerMenu";
import { PageProps } from "@/types";
import { ReactNode } from "react";
import AuthenticatedPartnerLayout from "./AuthenticatedPartnerLayout";

export default function PartnerLayout ({ children, auth, title }: PageProps<{ children: ReactNode, title: string }>) {
    return (
        <AuthenticatedPartnerLayout
            user={auth.user}
            header={
                <div className="flex items-center justify-between">
                    <h2 className="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">{title}</h2>
                    <PartnerMenu />
                </div>
            }
        >
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900 dark:text-gray-100">{children}</div>
                    </div>
                </div>
            </div>
        </AuthenticatedPartnerLayout>
    )
}