import Pagination from "@/Components/Pagination";
import Public from "@/Layouts/PublicLayout";
import { PublicProduct } from "@/classes";
import { LengthAwarePaginatorInterface, PageProps } from "@/types";

export default function Catalog ({ auth, ziggy, products }: PageProps<{ products: LengthAwarePaginatorInterface<PublicProduct> }>) {
    console.log(products.data);

    return (
        <Public auth={auth} ziggy={ziggy}>
            <div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
                <h2 className="text-2xl font-bold tracking-tight text-gray-200">Catalog</h2>
                <div className="mt-6 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                    {products.data.map((product: PublicProduct, index: number) => (
                        <div className="group relative" key={index}>
                            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
                                <img src={product.images.length ? product.images[0].url : ''} alt="Front of men&#039;s Basic Tee in black." className="h-full w-full object-cover object-center lg:h-full lg:w-full" />
                            </div>
                            <div className="mt-4 flex justify-between">
                                <div>
                                    <h3 className="text-sm text-gray-100">
                                        <a href="#">
                                            <span aria-hidden="true" className="absolute inset-0"></span>
                                            {product.name}
                                        </a>
                                    </h3>
                                    <p className="mt-1 text-sm text-gray-300">Sale</p>
                                </div>
                                <p className="text-sm font-medium text-gray-300">{product.currency + product.price}</p>
                            </div>
                        </div>
                    ))}
                </div>
                <Pagination links={products.links} />
            </div>
        </Public>
    )
}
