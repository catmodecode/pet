import Pagination from "@/Components/Pagination";
import ProductCardInList from "@/Components/ProductCardInList";
import PartnerLayout from "@/Layouts/PartnerLayout";
import { Product } from "@/classes";
import { LengthAwarePaginatorInterface, PageProps } from "@/types";
import { Link } from "@inertiajs/react";

export default function Products ({ auth, ziggy, products }: PageProps<{ products: LengthAwarePaginatorInterface<Product> }>) {
    const dataProducts = products.data.map((product: Product) => new Product(
        product.id,
        product.images,
        product.name,
        product.price,
        product.currency
    ))

    return (
        <PartnerLayout title="Products" auth={auth} ziggy={ziggy}>
            <Link href={route('partner-panel.products.create')}>
                +
            </Link>
            <div className="flex flex-col space-y-10">
                <div className="mt-6 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                    {
                        dataProducts.map((product: Product) => (<ProductCardInList key={product.id} product={product} />))
                    }
                </div>
                <Pagination links={products.links} />
            </div>
        </PartnerLayout>
    )
}