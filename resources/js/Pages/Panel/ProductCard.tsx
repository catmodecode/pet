import ProductCardEdit from "@/Components/ProductCardEdit";
import PartnerLayout from "@/Layouts/PartnerLayout";
import { Product } from "@/classes";
import { PageProps } from "@/types";

export default function ProductCard ({ product, auth, ziggy }: PageProps<{ product: Product | undefined }>) {
    return (
        <PartnerLayout title={product === undefined ? "New product" : "Edit product"} auth={auth} ziggy={ziggy}>
            <div>
                <ProductCardEdit product={product} className="m-auto" />
            </div>
        </PartnerLayout>
    )
}
