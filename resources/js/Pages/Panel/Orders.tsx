import OrderCard, { Order } from "@/Components/OrderCard";
import Authenticated from "@/Layouts/AuthenticatedLayout";
import PartnerLayout from "@/Layouts/PartnerLayout";
import { PageProps } from "@/types";

export default function Orders ({ auth, ziggy }: PageProps) {
    const order1 = new Order(
        new Date(),
        'https://3dnews.ru/assets/external/illustrations/2023/06/09/1088146/0.jpg',
        'created',
        'Lorem ipsum dolor sit amet',
    );
    return (
        <PartnerLayout title="Orders" auth={auth} ziggy={ziggy}>
            <div className="max-w-md mx-auto space-y-2 rounded-xl shadow-md overflow-hidden md:max-w-5xl m-5">
                <OrderCard order={order1} />
                <OrderCard order={undefined} />
                <OrderCard order={undefined} />
                <OrderCard order={undefined} />
            </div>
        </PartnerLayout>
    )
}