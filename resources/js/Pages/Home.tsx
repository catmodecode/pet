import { Link, Head } from '@inertiajs/react';
import { PageProps } from '@/types';
import Public from '@/Layouts/PublicLayout';

export default function Home ({ auth, ziggy }: PageProps<{}>) {
    return (
        <Public auth={auth} ziggy={ziggy}>
            <Head title="Welcome" />
            <img srcSet="/images/logo-500.webp 500w, /images/logo-700.webp 700w, /images/logo-1000.webp 1000w" className='max-w-screen-lg' />
            None of this
        </Public>
    );
}
