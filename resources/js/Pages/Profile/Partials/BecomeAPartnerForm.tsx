import { useRef, useState, FormEventHandler } from 'react';
import DangerButton from '@/Components/DangerButton';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import Modal from '@/Components/Modal';
import SecondaryButton from '@/Components/SecondaryButton';
import TextInput from '@/Components/TextInput';
import { useForm } from '@inertiajs/react';
import PrimaryButton from '@/Components/PrimaryButton';

export default function BecomeAPartnerForm ({ className = '' }: { className?: string }) {
    const [show, setShow] = useState(false);
    const nameInput = useRef<HTMLInputElement>(null);

    const {
        data,
        setData,
        post: post,
        processing,
        reset,
        errors,
    } = useForm({
        name: '',
    });

    const deleteUser: FormEventHandler = (e) => {
        e.preventDefault();

        post(route('api.register-as-partner'), {
            preserveScroll: true,
            onSuccess: () => setShow(false),
            onError: () => nameInput.current?.focus(),
            onFinish: () => reset(),
        });
    };

    return (
        <section className={`space-y-6 ${className}`}>
            <header>
                <h2 className="text-lg font-medium text-gray-900 dark:text-gray-100">Become a partner</h2>

                <p className="mt-1 text-sm text-gray-600 dark:text-gray-400">
                    Chose your company name and become a partner
                </p>
            </header>

            <PrimaryButton onClick={() => setShow(true)}>Register as a partner</PrimaryButton>

            <Modal show={show} onClose={() => setShow(false)}>
                <form onSubmit={deleteUser} className="p-6">
                    <h2 className="text-lg font-medium text-gray-900 dark:text-gray-100">
                        Partner
                    </h2>

                    <div className="mt-6">
                        <InputLabel htmlFor="password" value="Chose account name" className="sr-only" />

                        <TextInput
                            id="name"
                            type="name"
                            name="name"
                            ref={nameInput}
                            value={data.name}
                            onChange={(e) => setData('name', e.target.value)}
                            className="mt-1 block w-3/4"
                            isFocused
                            placeholder="Name"
                        />

                        <InputError message={errors.name} className="mt-2" />
                    </div>

                    <div className="mt-6 flex justify-end">
                        <SecondaryButton onClick={() => setShow(false)}>Cancel</SecondaryButton>

                        <PrimaryButton className="ms-3" disabled={processing}>
                            Register partner account
                        </PrimaryButton>
                    </div>
                </form>
            </Modal>
        </section>
    );
}