export default function (roles: string[], needle: string): boolean {
    if (!roles) {
        return false;
    }

    return roles.filter(
        (role: string) => role == needle
    ).length > 0
}