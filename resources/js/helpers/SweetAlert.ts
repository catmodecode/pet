import Swal from "sweetalert2";

export function swalError (title: string, html: string) {
    Swal.fire({
        title,
        html,
        icon: 'error',
    })
}