<?php

namespace Database\Factories;

use App\Models\Currency;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $currencies = Currency::all('code')
            ->map(fn(Currency $currency) => $currency->code)
            ->toArray();
        return [
            'name' => $this->faker->words(asText: true),
            'price' => rand(150, 500) * 10,
            'currency_code' => $this->faker->shuffleArray($currencies)[0]
        ];
    }
}
