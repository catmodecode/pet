<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Partner>
 */
class PartnerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $company = $this->faker->company();
        $created = $this->faker->dateTimeBetween('-2 years', 'now');
        return [
            'name' => $company,
            'slug' => Str::slug($company),
            'created_at' => $created,
            'updated_at' => $this->faker->dateTimeBetween($created, 'now'),
        ];
    }
}
