<?php

use App\Enums\Permissions;
use App\Enums\RoleNames;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Role::create(['name' => RoleNames::Root->value]);
        Role::create(['name' => RoleNames::Admin->value]);
        $partner = Role::create(['name' => RoleNames::Partner->value]);
        $partnerPanel = Permission::create(['name' => Permissions::PartnerPanel->value]);
        $partner->givePermissionTo($partnerPanel);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        $partner = Role::findByName(RoleNames::Partner->value);
        $partnerPanel = Permission::findByName(Permissions::PartnerPanel->value);
        $partner->revokePermissionTo($partnerPanel);
        $partnerPanel->delete();
        $partner->delete();
        Role::findByName(RoleNames::Admin->value)->delete();
        Role::findByName(RoleNames::Root->value)->delete();
    }
};
