<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->string('code')->primary();
            $table->timestamps();
            $table->string('name');
            $table->string('sign');
        });

        DB::table('currencies')->insert([
            [
                'code' => 'RUB',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'name' => 'rubbles',
                'sign' => '₽',
            ],
            [
                'code' => 'EUR',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'name' => 'euro',
                'sign' => '€',
            ],
            [
                'code' => 'USD',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'name' => 'dollar',
                'sign' => '$',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('currencies');
    }
};
