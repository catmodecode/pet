<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Region;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Region::all()->each(
            fn (Region $region) =>
            City::factory()
                ->count(10)
                ->state([
                    'country_id' => $region->country_id,
                    'region_id' => $region->id,
                ])
                ->create()
        );
    }
}
