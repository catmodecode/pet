<?php

namespace Database\Seeders;

use App\Models\Partner;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Partner::each(
            fn (Partner $partner) =>
            Product::factory(30, [
                'partner_id' => $partner->id
            ])->create()
        );
    }
}
