<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = new User();
        $user->name = env('DEFAULT_ADMIN_NAME');
        $user->email = env('DEFAULT_ADMIN_EMAIL');
        $user->password = Hash::make(env('DEFAULT_ADMIN_PASSWORD'));
        $user->email_verified_at = Carbon::now();
        $user->remember_token = Str::random(10);
        $user->save();

        User::factory()
            ->count(500)
            ->create();
    }
}
