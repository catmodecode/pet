<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Product;
use App\Services\Products\ProductImageService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Storage::disk('firebase')->delete('/images');
        $files = Storage::files('seedimages');
        $paths = [];
        foreach ($files as $file) {
            $data = Storage::get($file);

            $userId = 0;
            $path = implode('/', [
                'images',
                $userId,
                uniqid()
            ]);
            $paths[] = $path;

            Storage::disk('firebase')->put($path, $data);
        }

        Product::with(['partner.user'])->each(function (Product $product) use ($paths) {
            for ($i = 0; $i < 3; $i++) {
                $path = $paths[array_rand($paths)];

                $image = new Image();
                $image->user_id = $product->partner->user_id;
                $image->imageable_type = Product::class;
                $image->imageable_id = $product->id;
                $image->path = $path;
                $image->save();
            }
        });
    }
}
