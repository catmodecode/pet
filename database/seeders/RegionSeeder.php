<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Country::all()->each(
            fn (Country $country) =>
            Region::factory()
                ->count(10)
                ->state([
                    'country_id' => $country->id,
                ])
                ->create()
        );
    }
}
