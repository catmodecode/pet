<?php

namespace Database\Seeders;

use App\Models\Partner;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cmc = User::find(1);
        Partner::factory()
            ->state(['user_id' => $cmc->id])
            ->create();
        User::query()
            ->where('id', '!=', 1)
            ->inRandomOrder()
            ->limit(200)
            ->get()
            ->map(
                fn (User $user) =>
                Partner::factory()
                    ->state(['user_id' => $user->id])
                    ->create()
            );
    }
}
