<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ImageController extends Controller
{
    public function get(string $path): HttpResponse {
        // TODO: Переехать на s3
        $rendered_buffer = Cache::remember(
            'images-' . $path,
            60 * 24 * 10,
            fn () => Storage::disk('firebase')->get($path)
        );

        $response = Response::make($rendered_buffer);
        $response->header('Content-Type', 'image/png');
        $response->header('Cache-Control', 'max-age=2592000');

        return $response;
    }
}
