<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\Products\ProductsCatalogService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CatalogController extends Controller
{
    public function catalog(Request $request, ProductsCatalogService $productsCatalogService)
    {
        $page = $request->query('page', '1');
        $perPage = $request->query('perPage', '20');

        $products = $productsCatalogService->catalog($page, $perPage);
        return Inertia::render('Web/Catalog', ['products' => $products]);
    }
}
