<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\PartnerService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class PartnerRegistrationController extends Controller
{
    public function register(Request $request, PartnerService $partnerService): RedirectResponse
    {
        /**
         * @var User $user
         */
        $user = Auth::user();
        if (!is_null($user->partner)) {
            return Redirect::to(route('partner-panel.index'));
        }
        $name = $request->json('name');
        $partnerService->register($user, $name);

        return Redirect::to(route('partner-panel.index'));
    }
}
