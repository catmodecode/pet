<?php

namespace App\Http\Controllers\Partners;

use App\Dto\ProductDto;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use App\Services\PartnerService;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Leomax\Errors\Facades\SentryCaptureFacade;

class ProductsController extends Controller
{
    public function index(PartnerService $partnerService, Request $request)
    {
        $page = $request->query('page', 1);
        $perPage = $request->query('perPage', 8);
        /**
         * @var User $user
         */
        $user = Auth::user();
        $products = $partnerService->products($user->partner, $page, $perPage);
        $productsDto = ProductDto::fromLengthAwarePaginator($products);
        return Inertia::render('Panel/Products', [
            'products' => $productsDto,
        ]);
    }

    public function create()
    {
        $product = null;
        return Inertia::render('Panel/ProductCard', [
            'products' => $product,
        ]);
    }

    public function edit(Product $product)
    {
        $product = ProductDto::fromModel($product);
        SentryCaptureFacade::captureMessage('test1');
        return Inertia::render('Panel/ProductCard', [
            'product' => $product,
        ]);
    }
}
