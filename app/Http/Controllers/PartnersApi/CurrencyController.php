<?php

namespace App\Http\Controllers\PartnersApi;

use App\Dto\CurrencyDto;
use App\Http\Controllers\Controller;
use App\Services\CurrencyService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function list(
        CurrencyService $currencyService
    ): JsonResponse
    {
        $currencies = $currencyService->getAvailableForPartners();
        return new JsonResponse($currencies->toArray());
    }
}
