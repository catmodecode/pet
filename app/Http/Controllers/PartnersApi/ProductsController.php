<?php

namespace App\Http\Controllers\PartnersApi;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\Products\CreateProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    public function create(Request $request, CreateProductService $createProductService)
    {
        $request->validate([
            'title' => ['required', 'string'],
            'price' => ['required', 'numeric'],
            'currency' => ['required', 'string'],
            'images' => ['required', 'array'],
        ]);

        /** @vart User $user */
        $user = Auth::user();
        $partner = $user->partner;
        $images = array_map(fn($image) => $image['file'], $request->files->get('images'));
        $createProductService->create(
            $partner,
            $request->post('title'),
            floatval($request->post('price')),
            $request->post('currency'),
            $images,
        );

        return Redirect::to(route('partner-panel.products.index'));
    }
    public function save(Request $request, Product $product, CreateProductService $createProductService)
    {
        $request->validate([
            'title' => ['required', 'string'],
            'price' => ['required', 'numeric'],
            'currency' => ['required', 'string'],
            'newImages' => ['array'],
            'deletedImages' => ['array'],
        ]);

        /** @vart User $user */
        $user = Auth::user();
        $partner = $user->partner;
        $createProductService->edit(
            $product,
            $partner,
            $request->post('title'),
            floatval($request->post('price')),
            $request->post('currency'),
            $request->files->get('images'),
            $request->post('deletedImages')
        );

        return Redirect::to(route('partner-panel.products.index'));
    }
}
