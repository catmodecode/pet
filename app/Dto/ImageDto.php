<?php

namespace App\Dto;

use App\Models\Image;
use App\Services\Products\ProductImageService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class ImageDto
{
    public function __construct(
        public int $id,
        public string $url
    ) {
    }

    public static function fromModel(Image $image): self
    {
        /** @var ProductImageService $productImageService */
        $productImageService = App::make(ProductImageService::class);
        return new self(
            $image->id,
            $productImageService->getUrl($image->path, true),
        );
    }

    /**
     * @param Collection<Image> $collection
     * @return Collection
     */
    public static function fromCOllection(Collection $collection): Collection
    {
        return $collection->map(fn(Image $image) => self::fromModel($image));
    }
}