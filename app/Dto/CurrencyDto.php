<?php

namespace App\Dto;

use App\Models\Currency;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class CurrencyDto
{
    public function __construct(
        public readonly string $code,
        public readonly string $name,
        public readonly string $sign,
    ) {
    }

    public static function fromModel(Currency $currency): static
    {
        return new static(
            code: $currency->code,
            name: $currency->name,
            sign: $currency->sign,
        );
    }

    /**
     * fromModels
     *
     * @param Collection|array<Currency> $list
     * @return Collection|array<Currency>
     */
    public static function fromModels(Collection $list): Collection
    {
        return $list->map(fn(Currency $currency) => static::fromModel($currency));
    }
}
