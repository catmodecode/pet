<?php

namespace App\Dto;

use App\Models\Image;
use App\Models\Product;
use App\Services\Products\ProductImageService;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class PublicProductDto
{
    public function __construct(
        public readonly string $name,
        public readonly float $price,
        public readonly string $currency,
        public readonly array $images,
    ) {
    }

    public static function fromModel(Product $product): self
    {
        $productImageService = App::make(ProductImageService::class);
        return new self(
            name: $product->name,
            price: $product->price,
            currency: $product->currency->sign,
            images: $product->images->map(fn (Image $image) => $productImageService->getUrl($image->path))->toArray(),
        );
    }

    /**
     * Undocumented function
     *
     * @param LengthAwarePaginator<Product> $paginator
     * @return LengthAwarePaginator
     */
    public static function fromLengthAwarePaginator(LengthAwarePaginator $paginator): LengthAwarePaginator
    {
        $collection = $paginator->getCollection();
        $collection = $collection->map(fn (Product $product) => self::fromModel($product));
        $paginator->setCollection($collection);
        return $paginator;
    }
}