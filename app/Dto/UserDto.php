<?php

namespace App\Dto;

use App\Models\User;
use Carbon\Carbon;
use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Contracts\Role;

class UserDto
{
    public function __construct(
        public readonly int $id,
        public readonly Carbon $created_at,
        public readonly Carbon $updated_at,
        public readonly string $email,
        public readonly ?Carbon $email_verified_at,
        public readonly string $name,
        public readonly array $roles,
        public readonly array $permissions,
    ) {
    }

    public static function fromModel(User $user): static
    {
        return new static(
            $user->id,
            $user->created_at,
            $user->updated_at,
            $user->email,
            $user->email_verified_at,
            $user->name,
            $user->roles->map(fn (Role $role) => $role->name)->toArray(),
            $user->permissions->map(fn (Permission $permission) => $permission->name)->toArray(),
        );
    }
}
