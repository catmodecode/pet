<?php

namespace App\Dto;

use App\Models\Image;
use App\Models\Product;
use App\Services\Products\ProductImageService;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

/**
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $currency
 * @property ImageDto[] $images
 */
class ProductDto
{
    public function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly float $price,
        public readonly string $currency,
        public readonly array $images,
    ) {
    }

    public static function fromModel(Product $product): self
    {
        /** @var ProductImageService $productImageService */
        $productImageService = App::make(ProductImageService::class);
        return new self(
            id: $product->id,
            name: $product->name,
            price: $product->price,
            currency: $product->currency->code,
            images: ImageDto::fromCOllection($product->images)->toArray(),
        );
    }

    public static function fromLengthAwarePaginator(LengthAwarePaginator $paginator): LengthAwarePaginator
    {
        $collection = $paginator->getCollection();
        $collection = $collection->map(fn (Product $product) => self::fromModel($product));
        $paginator->setCollection($collection);
        return $paginator;
    }
}