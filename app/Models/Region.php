<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @var int $id
 * @var int $country_id
 * @var string $name
 */
class Region extends Model
{
    use HasFactory;

    public $timestamps = false;
}
