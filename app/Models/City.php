<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @var int $id
 * @var int $country_id
 * @var int $region_id
 * @var string $name
 * @var string $slug
 */
class City extends Model
{
    use HasFactory;

    public $timestamps = false;
}
