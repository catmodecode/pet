<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $code
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 * @property string $sign
 */
class Currency extends Model
{
    use HasFactory;
}
