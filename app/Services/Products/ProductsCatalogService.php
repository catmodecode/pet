<?php

namespace App\Services\Products;

use App\Dto\PublicProductDto;
use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ProductsCatalogService
{
    public function catalog(int $page, int $perPage): LengthAwarePaginator {
        $products = Product::with(['currency', 'images'])
            ->paginate(page: $page, perPage: $perPage);
        return PublicProductDto::fromLengthAwarePaginator($products);
    }
}