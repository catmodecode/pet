<?php

namespace App\Services\Products;

use App\Models\Image;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Exceptions\UnauthorizedException;

class ProductImageService
{
    private const IMAGES_PATH = 'images';
    public function storeImage(string $data, int|User $user, Product|int $product): Image
    {
        $userId = is_int($user)
            ? $user
            : $user->id;

        $productId = is_int($product)
            ? $product
            : $product->id;

        $path = implode('/', [
            self::IMAGES_PATH,
            $userId,
            uniqid()
        ]);

        Storage::disk('firebase')->put($path, $data);

        $image = new Image();
        $image->user_id = $userId;
        $image->imageable_type = Product::class;
        $image->imageable_id = $productId;
        $image->path = $path;
        $image->save();

        return $image;
    }

    public function getUrl(string $path, bool $absolute = false): string
    {
        return route('web.images', ['path' => $path], $absolute);
    }

    /**
     * Summary of detachAndDeleteImage
     * @param int|\App\Models\Image $image
     * @param int|\App\Models\User $user
     * @throws \Kreait\Firebase\Exception\Auth\OperationNotAllowed
     * @return void
     */
    public function detachAndDeleteImage(int|Image $image, int|User $user): void
    {
        $userId = is_int($user)
            ? $user
            : $user->id;

        $image = is_int($image)
            ? Image::find($image)
            : $image;

        if ($image->user_id !== $userId) {
            throw new UnauthorizedException(403, 'User has no rights to delete this image');
        }
        $image->delete();
    }
}
