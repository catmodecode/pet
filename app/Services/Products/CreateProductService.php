<?php

namespace App\Services\Products;

use App\Models\Currency;
use App\Models\Image;
use App\Models\Partner;
use App\Models\Product;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CreateProductService
{
    /**
     * Summary of create
     * @param \App\Models\Partner|int $partner
     * @param string $name
     * @param string $localImage
     * @param UploadedFile[] $images
     * @return void
     */
    public function create(
        Partner|int $partner,
        string $name,
        float $price,
        string|Currency $currency,
        array $images,
    ): Product {
        $product = new Product();
        $product->name = $name;
        $product->price = $price;
        $product->partner_id = $partner->id;
        $product->currency_code = is_string($currency)
            ? $currency
            : $currency->code;

        $product->save();

        /** @var ProductImageService $imageService */
        $imageService = App::make(ProductImageService::class);

        foreach ($images as $image) {
            $imageService->storeImage(
                file_get_contents($image->getRealPath()),
                $partner->user_id,
                $product
            );
        }

        return $product;
    }

    /**
     * Summary of create
     * @param \App\Models\Partner|int $partner
     * @param string $name
     * @param string $localImage
     * @param UploadedFile[] $newImages
     * @param int[] $deletedImages
     * @return void
     */
    public function edit(
        Product|int $product,
        Partner|int $partner,
        string $name,
        float $price,
        string|Currency $currency,
        array $images,
        array $deletedImages,
    ): Product {
        $product->name = $name;
        $product->price = $price;
        $product->partner_id = $partner->id;
        $product->currency_code = is_string($currency)
            ? $currency
            : $currency->code;

        $product->save();

        /** @var ProductImageService $imageService */
        $imageService = App::make(ProductImageService::class);

        foreach ($images as $image) {
            $imageService->storeImage(
                file_get_contents($image->getRealPath()),
                $partner->user_id,
                $product
            );
        }

        $deletedImages = Image::whereIn('id', $deletedImages)->get();

        foreach ($deletedImages as $deletedImage) {
            $imageService->detachAndDeleteImage(
                $deletedImage,
                $partner->user_id,
            );
        }

        return $product;
    }
}
