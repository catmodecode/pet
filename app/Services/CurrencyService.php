<?php

namespace App\Services;

use App\Dto\CurrencyDto;
use App\Models\Currency;
use Doctrine\Common\Cache\Cache;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache as FacadesCache;

class CurrencyService
{
    private const PARTNER_CURRENCIES_KEY = 'partner_currencies';
    private const PRICE_KEY = 'currency_';

    public function getAvailableForPartners(): Collection
    {
        return FacadesCache::driver('array')->remember(
            self::PARTNER_CURRENCIES_KEY,
            0,
            fn () => CurrencyDto::fromModels(Currency::all())
        );
    }
}
