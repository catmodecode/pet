<?php

namespace App\Services;

use App\Models\Partner;
use App\Models\Product;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

final class PartnerService
{
    private const PARTNER_KEY = 'partner_';
    public function find(int $id): Partner
    {
        return Cache::driver('array')->remember(self::PARTNER_KEY . $id, 0, fn () => Partner::find($id));
    }

    /**
     * Summary of products
     * @param int|Partner $partner
     * @return LengthAwarePaginator<int, Product>
     */
    public function products(int|Partner $partner, int $page = 1, int $perPage = 10): LengthAwarePaginator
    {
        $baseQuery = is_int($partner)
            ? $partner = Partner::where('id', '=', $partner)
            ->first()->products()
            : $partner = $partner->products();
        $products = $baseQuery->orderBy('id', 'desc')
            ->with(['currency', 'images'])
            ->paginate(perPage: $perPage, page: $page);
        return $products;
    }

    public function register(User|int $user, string $name): Partner
    {
        $partner = new Partner();
        $partner->user_id = is_int($user)
            ? $user
            : $user->id;
        $partner->name = $name;

        if (Partner::where('slug', '=', Str::slug($name))->exists()) {
            $partner->slug = Str::slug($name) . '_' . $user->id;
        } else {
            $partner->slug = Str::slug($name);
        }

        $partner->save();

        return $partner;
    }
}
