<?php

namespace App\Enums;

enum RoleNames: string
{
    case Root = 'root';
    case Admin = 'admin';
    case Partner = 'partner';
}
