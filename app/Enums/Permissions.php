<?php

namespace App\Enums;

enum Permissions: string
{
    case PartnerPanel = 'partner panel';
}
