<?php

namespace App\Providers;

use App\Services\FirebaseRealtimeDatabaseAdapter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        app(FilesystemManager::class)->extend(
            'firebase',
            fn(Application $app, array $config) =>
            app(FirebaseRealtimeDatabaseAdapter::class)
        );
    }
}
