<?php

namespace App\Providers;

use App\Enums\RoleNames;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to your application's "home" route.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     */
    public function boot(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(200)->by($request->user()?->id ?: $request->ip());
        });

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->as('api.')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->as('web.')
                ->group(base_path('routes/web.php'));

            Route::middleware(['web', 'auth', 'role:' . RoleNames::Partner->value, 'verified'])
                ->prefix('/panel')
                ->as('partner-panel.')
                ->group(base_path('routes/partners.php'));

            Route::middleware(['api', 'auth', 'role:' . RoleNames::Partner->value, 'verified'])
                ->prefix('/partner-api')
                ->as('partner-api.')
                ->group(base_path('routes/partners_api.php'));

            Route::middleware([
                'auth',
                'role:' . RoleNames::Root->value . ',' . RoleNames::Admin->value,
                'verified'
            ])
                ->prefix('/admin')
                ->as('admin.')
                ->group(base_path('routes/admin.php'));
        });
    }
}
