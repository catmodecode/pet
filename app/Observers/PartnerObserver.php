<?php

namespace App\Observers;

use App\Enums\RoleNames;
use App\Models\Partner;
use App\Models\User;

class PartnerObserver
{
    public function created(Partner $partner)
    {
        $sessionTeamId = getPermissionsTeamId();
        $user = $partner->user;
        setPermissionsTeamId($partner);
        $user->assignRole(RoleNames::Partner->value);
        setPermissionsTeamId($sessionTeamId);
        $user->saveQuietly();
    }
    
    public function deleted(Partner $partner)
    {
        $sessionTeamId = getPermissionsTeamId();
        $user = $partner->user;
        setPermissionsTeamId($partner);
        $user->removeRole(RoleNames::Partner->value);
        setPermissionsTeamId($sessionTeamId);
        $user->saveQuietly();
    }
    
}
