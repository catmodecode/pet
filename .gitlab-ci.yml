image: docker:24.0.5

services:
  - docker:24.0.5-dind

stages:
  - common_build
  - build
  - nginx_build
  - deploy_php
  - deploy_nginx

variables:
  DOCKER_HOST: unix:///var/run/docker.sock
  COMMON_IMAGE_NAME: registry.gitlab.com/catmodecode/pet/common
  IMAGE_NAME: registry.gitlab.com/catmodecode/pet
  NGINX_IMAGE_NAME: registry.gitlab.com/catmodecode/pet/nginx
  APP_CONTAINER: my_php
  APP_CONTAINER_INERTIA: my_inertia
  NGINX_CONTAINER: my_nginx

before_script:
  - docker info
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  - eval $(ssh-agent -s)
  - chmod 400 "$SSH_PRIVATE_KEY"
  - ssh-add "$SSH_PRIVATE_KEY"
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh

common_build:
  stage: common_build
  script:
    - cp $ENV_PHP ${CI_PROJECT_DIR}/.env
    - whoami
    - docker ps
    - docker build --build-arg "PIPELINE_ID=$CI_PIPELINE_IID"  -f Dockerfile.common -t $COMMON_IMAGE_NAME:$CI_PIPELINE_IID .
    - docker tag $COMMON_IMAGE_NAME:$CI_PIPELINE_IID $COMMON_IMAGE_NAME:latest
    - docker push $COMMON_IMAGE_NAME:$CI_PIPELINE_IID
    - docker push $COMMON_IMAGE_NAME:latest
  environment:
    name: $CI_COMMIT_REF_SLUG
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: always
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: always
    - when: never

build:
  stage: build
  needs:
    - common_build
  script:
    - docker build --build-arg "PIPELINE_ID=$CI_PIPELINE_IID" -t $IMAGE_NAME:$CI_PIPELINE_IID .
    - docker tag $IMAGE_NAME:$CI_PIPELINE_IID $IMAGE_NAME:latest
    - docker push $IMAGE_NAME:$CI_PIPELINE_IID
    - docker push $IMAGE_NAME:latest
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: on_success
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: on_success
    - when: never

nginx_build:
  stage: nginx_build
  needs:
    - common_build
  script:
    - docker build --build-arg "PIPELINE_ID=$CI_PIPELINE_IID" -f Dockerfile.nginx -t $NGINX_IMAGE_NAME:$CI_PIPELINE_IID .
    - docker tag $NGINX_IMAGE_NAME:$CI_PIPELINE_IID $NGINX_IMAGE_NAME:latest
    - docker push $NGINX_IMAGE_NAME:$CI_PIPELINE_IID
    - docker push $NGINX_IMAGE_NAME:latest
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: on_success
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      when: on_success
    - when: never
#  when: manual

deploy:
  stage: deploy_php
  needs:
    - build
  script:
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker pull $IMAGE_NAME:$CI_PIPELINE_IID"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker rm -f ${APP_CONTAINER}_new ${APP_CONTAINER_INERTIA}_new || true"
    - |
      ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker run -e SENTRY_RELEASE=$CI_PIPELINE_IID -d --name ${APP_CONTAINER_INERTIA}_new \
      -v /srv/storage:/app/storage \
      --network=app \
      $IMAGE_NAME:$CI_PIPELINE_IID ./artisan inertia:start-ssr"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker rm -f $APP_CONTAINER_INERTIA || true"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker rename ${APP_CONTAINER_INERTIA}_new $APP_CONTAINER_INERTIA"
    - |
      ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker run -e SENTRY_RELEASE=$CI_PIPELINE_IID -d --name ${APP_CONTAINER}_new \
      -v /srv/storage:/app/storage \
      --network=app \
      $IMAGE_NAME:$CI_PIPELINE_IID"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "bash -s" < etc/deploy.sh
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker rm -f $APP_CONTAINER || true"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker rename ${APP_CONTAINER}_new $APP_CONTAINER"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker exec my_nginx sh -c \"nginx -t && nginx -s reload\""
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker tag $IMAGE_NAME:$CI_PIPELINE_IID $IMAGE_NAME:cron"
  environment:
    name: $CI_COMMIT_REF_SLUG
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: on_success
    - when: never

deploy_nginx:
  stage: deploy_nginx
  needs:
    - nginx_build
  script:
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker pull $NGINX_IMAGE_NAME:$CI_PIPELINE_IID"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker rm -f ${NGINX_CONTAINER}_new || true"
    - |
      ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker run -d --name ${NGINX_CONTAINER}_new \
      -v /srv/storage:/app/storage \
      --network=app \
      ${NGINX_IMAGE_NAME}:${CI_PIPELINE_IID}"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker rm -f $NGINX_CONTAINER || true"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker rename ${NGINX_CONTAINER}_new $NGINX_CONTAINER"
    - ssh -o StrictHostKeyChecking=no -p $DEPLOY_PORT $DEPLOY_USER@$DEPLOY_HOST "docker compose exec nginx_proxy sh -c \"nginx -t && nginx -s reload\""
  environment:
    name: $CI_COMMIT_REF_SLUG
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: on_success
    - when: never
